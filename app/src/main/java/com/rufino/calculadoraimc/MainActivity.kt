package com.rufino.calculadoraimc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import com.rufino.calculadoraimc.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bt_calcular = binding.btCalcular
        val mensagem = binding.mensagem


        bt_calcular.setOnClickListener {
            val editPeso = binding.editPeso.text.toString()
            val editAltura = binding.editAltura.text.toString()

            if (editPeso.isEmpty()){
                mensagem.setText("Entre com peso")
            }else if (editAltura.isEmpty()){
                mensagem.setText("Entre com altura")
            }else {
                CalculoIMC()
            }

        }

    }
    private fun CalculoIMC(){
        val editPeso = Integer.parseInt(binding.editPeso.text.toString())
        val editAltura = java.lang.Float.parseFloat(binding.editAltura.text.toString())
        val resultado = binding.mensagem
        val imc = editPeso / ( editAltura * editAltura)

        val Mensagem = when{
            imc <=18.5 -> "Peso Baixo"
            imc <=24.9 -> "Peso Normal"
            imc <=29.9 -> "Sobre Peso"
            imc <=34.9 -> "Obesidade Grau I"
            imc <=39.9 -> "Obesidade Grau II"
            else -> "Obesidade Morbida Grau III"
        }

        resultado.setText("IMC: $imc \n $Mensagem")

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_principal, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.reset -> {
                val limparEditPeso = binding.editPeso
                val limparEditAltura = binding.editAltura
                limparEditPeso.setText("")
                limparEditAltura.setText("")
                }
        }
        return super.onOptionsItemSelected(item)
    }
}